<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('api.auth',['except' => ['index','show']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        return response()->json([
            'status' => 'success',
            'code' => 200,
            'categories' => $categories
        ],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Recoger los datos por Post
        $json = $request->input('json');
        $params_array = json_decode($json,true);

        if(empty($params_array)){
            $data = array(
                'status' => 'error',
                'code' => 400,
                'message' => 'No se ha guardado la categoría'
            );
        }else{
            //Validar los datos
            $validate = \Validator::make($params_array,[
                'name' => 'required'
            ]);

            if($validate->fails()){
                $data = array(
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'No se ha guardado la categoría',
                    'errors' => $validate->errors()
                );
            }else{
                //Guardar la categoria
                $category = new Category();

                $category->name = $params_array['name'];
                $category->save();

                //Devolver el resultado
                $data = array(
                    'status' => 'success',
                    'code' => 201,
                    'message' => 'Se a creado la categoría',
                    'category' => $category
                );
            }
        }

        return response()->json($data,$data['code']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);
        if(is_object($category)){
            return response()->json([
                'status' => 'success',
                'code' => 200,
                'categories' => $category
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'code' => 404,
                'message' => 'No existe la categoría'
            ],404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Obtener los datos del post
        $json = $request->input('json');
        $params_array = json_decode($json,true);
        if(!empty($params_array)){
            //validar los datos
            $validate = \Validator::make($params_array,[
                'name' => 'required'
            ]);

            if($validate->fails()){
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'message' => 'Datos incorrectos',
                    'errors' => $validate->errors()
                );
            }else{
                //Quitar lo que no quiero actualizar
                unset($params_array['created_at']);
                unset($params_array['id']);

                //actualizar los datos
                $category = new Category();
                $category->where('id',$id)->update($params_array);

                //devolver los datos
                $data = array(
                    'status' => 'success',
                    'code' => 201,
                    'message' => 'Se actualizó la categoria',
                    'category' => $category->find($id)
                );

            }
        }else{
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'Datos incorrectos'
            );
        }

        return response()->json($data,$data['code']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = new Category();
        $category -> destroy($id);

        return response()->json([
            "status" => "success",
            "code" => 200,
            "message" => "La categoria fue eliminada exitosamente"
        ],200);
    }
}
