<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Post;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('api.auth',['except' => ['index','show','getImage','getPostsByCategory','getPostsByUser']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::paginate(10)->load('category')->load('user');

        if(is_null($posts)){
            return response()->json([
                'status' => 'error',
                'code' => 400,
                'message' => 'No hay posts'
            ],200);
        }else{
            return response()->json([
                'status' => 'success',
                'code' => 200,
                'posts' => $posts
            ],200);
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //obtener los datos del post
        $json = $request->input('json',null);
        $params = json_decode($json);
        $params_array = json_decode($json,true);

        if(is_array($params_array)){

            //consegir los datos del usuario identificado
            $user = $this->getIdentity($request);

            //validar los datos
            $validate = \Validator::make($params_array,[
                'title' => 'required',
                'content' => 'required',
                'category_id' => 'required|integer',
                'image' => 'required'
            ]);

            if($validate->fails()){
                $data = [
                    'status' => 'error',
                    'code' => '400',
                    'message' => 'Faltan datos',
                    'errors' => $validate->errors()
                ];
            }else{
                //guardar los datos
                $post = new Post();
                $post->user_id = $user->sub;
                $post->category_id = $params->category_id;
                $post->title = $params->title;
                $post->content = $params->content;
                $post->image = $params->image;
                $post->save();

                $data = [
                    'status' => 'success',
                    'code' => '201',
                    'message' => 'Se creó el post',
                    'post' => $post
                ];
            }
        }else{
            $data = [
                'status' => 'error',
                'code' => '400',
                'message' => 'Se enviaron incorrecto los datos'
            ];
        }

        //devolver los datos
        return response()->json($data,$data['code']);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id)->load('category')->load('user');

        if(is_null($post)){
            return response()->json([
                'status' => 'error',
                'code' => 400,
                'message' => 'No existe ese post'
            ],200);
        }else{
            return response()->json([
                'status' => 'success',
                'code' => 200,
                'posts' => $post
            ],200);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    //Subir imagenes
    public function upload(Request $request)
    {
        //recoger la imagen de la petición
        $image = $request->file('file0',null);

        //validar la imagen
        $validate = \Validator::make($request->all(),[
           'file0' => 'image|required|mimes:jpg,jpeg,png,gif'
        ]);

        if(!$image || $validate->fails()){
            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'Error al subir la imagen'
            ];
        }else{
            //guardar imagen en disco
            $image_name = time().$image->getClientOriginalName();
            \Storage::disk('images')->put($image_name,\File::get($image));

            $data = [
                'status' => 'success',
                'code' => 200,
                'image' => $image_name
            ];
        }

        //Devolver datos
        return response()->json($data,$data['code']);
    }

    public function getImage($filename){

        // comprobar si existe el fichero
        $isset = \Storage::disk('images')->exists($filename);

        if($isset){
            // conseguir la imagen
            $file = \Storage::disk('images')->get($filename);
            //Devolver la imagen
            return new Response($file,200);
        }else {

            //Mostrar error
            $data = [
                'status' => 'error',
                'code' => 404,
                'message' => 'No existe la imagen'
            ];
            return response()->json($data,$data['code']);
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //Obtener los datos
        $json = $request->input('json',null);
        $params_array = json_decode($json,true);

        if(!empty($params_array)){
            //validar datos
            $validate = \Validator::make($params_array,[
                'title' => 'required',
                'content' => 'required',
                'category_id' => 'required|integer'
            ]);

            if($validate->fails()){
                $data = [
                    'status' => 'error',
                    'code' => 400,
                    'message' => 'Faltan datos',
                    'errors' => $validate->errors()
                ];
            }else{
                //Quitar datos que no se requieren actualizar
                unset($params_array['id']);
                unset($params_array['user_id']);
                unset($params_array['created_at']);
                unset($params_array['user']);

                //usuario identificado
                $user = $this->getIdentity($request);

                //Guardar los cambios
                $post = Post::where('id',$id)->where('user_id',$user->sub)->update($params_array);

                //confirmamos si se actualizó
                if($post){
                    $data = [
                        'status' => 'success',
                        'code' => 200,
                        'message' => 'Se actualizo la información',
                        'post' => Post::find($id)
                    ];
                }else{
                    $data = [
                        'status' => 'error',
                        'code' => 400,
                        'message' => 'No se actualizo'
                    ];
                }
            }

        }else{

            $data = [
                'status' => 'error',
                'code' => 400,
                'message' => 'No hay datos!'
            ];

        }

        //Devolver los datos
        return response()->json($data,$data['code']);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $user = $this->getIdentity($request);
        //Buscar el post
        $post = Post::where('id',$id)
                    ->where('user_id',$user->sub)
                    ->first();

        //Verificar si existe
        if(!empty($post)){
            //elimnar post
            $post -> delete();

            $data = [
              'status' => 'success',
                'code' => '200',
                'message' => 'Post eliminado',
                'post' => $post
            ];
        }else{
            $data = [
                'status' => 'error',
                'code' => '200',
                'message' => 'No existe el post'
            ];
        }

        //devolver los datos
        return response()->json($data,$data['code']);
    }

    private function getIdentity(Request $request){
        //conseguir usuario identificado
        $token = $request->header('Authorization',null);
        $jwtAuth = new \JwtAuth();
        $user = $jwtAuth->checkToken($token,true);

        return $user;
    }

    public function getPostsByCategory($id){

        $posts = Post::where('category_id',$id)->paginate(5);

        return response()->json([
            'status' => 'success',
            'code' => '200',
            'posts' => $posts
        ],200);

    }

    public function getPostsByUser($id){

        $posts = Post::where('user_id',$id)->paginate(5);

        return response()->json([
            'status' => 'success',
            'code' => '200',
            'posts' => $posts
        ],200);

    }

}
