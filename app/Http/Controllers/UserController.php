<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\User;

class UserController extends Controller
{
    //
    public function pruebas(Request $request){

        return "Acción de pruebas de UserController";

    }

    public function register(Request $request){

        //Recoger los datos del usuario por post
        $json = $request->input('json',null);
        $params = json_decode($json); //objeto
        $params_array = json_decode($json,true); //array

        if(!empty($params) && !empty($params_array)){
            //Limpiar datos
            $params_array = array_map('trim',$params_array);

            //Validar los datos
            $validate = \Validator::make($params_array,[
                'name'          => 'required|alpha',
                'surname'       => 'required|alpha',
                'email'         => 'required|email|unique:users', // unique:tabla
                'password'      => 'required'
            ]);

            if($validate->fails()){
                //La validación tuvo un error.
                $data = array(
                    'status' => 'error',
                    'code' => 404,
                    'errors' => $validate->errors(),
                    'message' => 'El usuario no se ha creado'
                );
                return response()->json($data,$data['code']);
            }else{
                //Validación correcta

                //Cifrar la contraseña.
                $pwd = hash('sha256',$params->password);

                //Crear el usuario
                $user = new User();
                $user -> name = $params_array['name'];
                $user -> surname = $params_array['surname'];
                $user -> email = $params_array['email'];
                $user -> password = $pwd;
                $user -> role = 'ROLE_USER';

                //Guardar el usuario
                $user->save();

                $data = array(
                    'status' => 'success',
                    'code' => 201,
                    'message' => 'El usuario se ha creado correctamente',
                    'user' => $user
                );
                return response()->json($data,$data['code']);


            }
        }else{
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'Los datos enviados no son correctos'
            );
            return response()->json($data,$data['code']);
        }


    }

    public function login(Request $request){

        $jwtAuth = new \JwtAuth;

        //Recibir datos por POST
        $json = $request->input('json');
        $params = json_decode($json);
        $params_array = json_decode($json,true);

        // Validar esos datos
        $validate = \Validator::make($params_array,[
           'email'      => 'email|required',
            'password'  => 'required'
        ]);


        if($validate->fails()){
            //La validación tuvo un error.
            $signup = array(
                'status' => 'error',
                'code' => 404,
                'errors' => $validate->errors(),
                'message' => 'Login incorrecto'
            );
            return response()->json($signup,$signup['code']);
        }else{

            //Cifrar la password
            $pwd = hash('sha256',$params->password);
            $signup = $jwtAuth->signup( $params->email,$pwd );

            //Devolver token o datos
            if(!empty($params->getToken)){
                $signup = $jwtAuth->signup( $params->email,$pwd,true );
            }

        }

        return response()->json($signup,200);

    }

    public function update(Request $request){

        //Recoger los datos por post
        $json = $request->input('json',null);
        $params_array = json_decode($json,true);

        if(!empty($params_array)){

            //Sacar usuario identificado
            $token = $request -> header('Authorization');
            $jwtAuth = new \JwtAuth();
            $user = $jwtAuth->checkToken($token,true);

            //válidar los datos
            $validate = \Validator::make($params_array,[
                'name'          => 'required|alpha',
                'surname'       => 'required|alpha',
                'email'         => 'required|email|unique:users,email,'.$user->sub.',id'// unique:tabla
            ]);

            if($validate->fails()){
                //No pasa la validación
                $data = array(
                    'status' => 'error',
                    'message' => 'No se puede actualizar',
                    'code' => 400,
                    'errors' => $validate->errors()
                );

            }else{
                //Quitar los campos que no se quieren actualizar
                unset($params_array['id']);
                unset($params_array['role']);
                unset($params_array['password']);
                unset($params_array['created_at']);
                unset($params_array['remember_token']);

                //Actualizar el usuario en la base de datos
                $user_update = User::where('id',$user->sub)->update($params_array);

                //devolver un array con resultado
                $data = array(
                    'status' => 'success',
                    'message' => 'El usuario ha sido actualizado',
                    'code' => 200,
                    'user' => $user_update,
                    'change' => $params_array
                );
            }

        }else{
            //el usuario no esta identificado
            $data = array(
              'status' => 'error',
              'message' => 'El usuario no esta identificado',
              'code' => 400
            );
        }

        return response()->json($data,$data['code']);
    }

    public function upload(Request $request){

        // Recoger los datos de la aplicación
        $image = $request->file('file0');


        // Validación de la imagen
        $validate = \Validator::make($request->all(),[
           'file0' => 'required|image|mimes:jpg,jpeg,png,gif'
        ]);

        // Guardar la imagen
        if(!$image || $validate->fails()){
            $data = array(
                'status' => 'error',
                'message' => 'Error al subir la imagen',
                'errors' =>  $validate->errors(),
                'code' => 400
            );
        }else{
            $image_name = time().$image->getClientOriginalName();
            \Storage::disk('users')->put($image_name,\File::get($image));

            $data = array(
                'status' => 'success',
                'code' => 200,
                'image' => $image_name
            );
        }

        return response()->json($data,$data['code']);

    }

    public function getImage($filename){
        $isset = \Storage::disk('users')->exists($filename);
        if($isset){

            $file = \Storage::disk('users')->get($filename);
            return new Response($file,200);

        }else{
            $data = array(
                'status' => 'error',
                'code' => 404,
                'message' => 'La imagen no existe'
            );
            return response()->json($data,$data['code']);
        }
    }

    public function detail($id){

        $user = User::find($id);

        if(is_object($user)){

            $data = array(
                'code' => 200,
                'status' => 'success',
                'user' => $user
            );

        }else{
            $data = array(
                'code' => 404,
                'status' => 'error',
                'message' => 'El usuario no existe'
            );
        }

        return response()->json($data,$data['code']);

    }

}