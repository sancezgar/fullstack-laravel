CREATE DATABASE IF NOT EXISTS api_blog;

use api_blog;

CREATE TABLE users(

  id                  int auto_increment not null,
  name                varchar(50) not null,
  surname             varchar(100),
  role                varchar(20),
  email               varchar(255) not null,
  password            varchar(255) not null,
  description         text,
  image               varchar(255),
  created_at          datetime default null,
  updated_at          datetime default null,
  remember_token      varchar(255),
  CONSTRAINT pk_users PRIMARY KEY(id)

) ENGINE=InnoDB;

CREATE TABLE categories(

  id                        int auto_increment not null,
  name                      varchar(50) not null,
  created_at                datetime default null,
  updated_at                datetime default null,
  CONSTRAINT pk_categories  PRIMARY KEY(id)

)ENGINE=InnoDB;

CREATE TABLE posts(

  id            int auto_increment not null,
  user_id       int not null,
  category_id   int not null,
  title         varchar(255),
  content       text,
  image         varchar(255),
  create_at     datetime default null,
  update_at     datetime default null,
  CONSTRAINT pk_posts PRIMARY KEY(id),
  CONSTRAINT fk_post_user FOREIGN KEY (user_id) REFERENCES users(id),
  CONSTRAINT fk_post_category FOREIGN KEY (category_id) REFERENCES categories(id)

)ENGINE=InnoDB